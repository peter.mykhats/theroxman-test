---
projects:
  - type: "1"
    title: "Smarter Golf Club Technology"
    text: "The leading provider of full suite membership and club management solutions to country, golf, city, yacht and other private clubs."
    bgColorClass: "lightGreen"
    img: clubessential_laptop.png
    imgTabPro: clubessential_laptop.png
    imgTab: clubessential_tablet.png
    imgTabMob: clubessential_tablet.png
    imgMob: clubessential_mobile.png
    hashtag: "# sport"
  - image: /img/404.png
    type: "2"
    title: "Project 5  type 2 "
  - image: /img/404.png
    type: "3"
    title: "Project 3 (shopcart image) type 3 "
  - image: /img/home-sets.png
    type: "3"
    title: project 1 (Sun image)  type 3
  - image: /img/404.png
    type: "4"
    title: "Project 4  type 4 "
---

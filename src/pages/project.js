import React from 'react'
import Layout from '../layout'
import Hero from '../components/projectPage/Hero'
import HeroDetail from '../components/projectPage/HeroDetail'
import Prototyping from '../components/projectPage/Prototyping'
import Philosophy from '../components/projectPage/Philosophy'
import FullImage from '../components/projectPage/FullImage'
import CameraInteraction from '../components/projectPage/CameraInteraction'
import CloudStudio from '../components/projectPage/CloudStudio'

const Project = () => {
  return (
    <>
      <Layout>
        <Hero />
        <HeroDetail />
        <Prototyping />
        <Philosophy />
        <FullImage
          img='04_laptop.png'
          imgTab='04_tablet.png'
          imgMob='04_mobile.png'
        />
        <CameraInteraction />
        <FullImage
          img='06_laptop.png'
          imgTab='06_tablet.png'
          imgMob='06_mobile.png'
        />
        <CloudStudio />
      </Layout>
    </>
  )
}

export default Project

import React from "react";
import Layout from "../layout";
import { graphql } from "gatsby";
// import TestBlock from "../components/homePage/TestBlockOne";
// import TestBlock_1 from "../components/homePage/TestBlockOne";
import TestBlock_2 from "../components/homePage/TestBlockTwo";
import TestBlock_3 from "../components/homePage/TestBlockThree";
import TestBlock_4 from "../components/homePage/TestBlockFour";
import TestBlock_5 from "../components/homePage/TestBlockFive";
import Hero from "../components/homePage/Hero";
import Process from "../components/homePage/Process";
import Card from "../components/modules/Card";
// import Cards from '../components/modules/Cards'
// import BoxWithTitle from '../components/modules/BoxWithTitle'
// import Box from '../components/modules/Box'
// import Branding from '../components/homePage/Branding'
// import List from '../components/homePage/List'
// import Form from '../components/homePage/Form'
// import Modal from '../components/modules/Modal'

const IndexPage = ({ data }) => {
  // const [isShowing, setIsShowing] = useState(false)
  // const toggle = () => {
  //   setIsShowing(!isShowing)
  // }
  const { edges } = data.allMarkdownRemark;
  const list = edges.map(item => item.node);
  const blockList = list[0].frontmatter.projects;
  console.log("blockList::", blockList);
  const rnd = Date.now().toString();
  console.log(rnd);

  const type = blockList.map((item, index) => {
    switch (item.type) {
      case "1":
        return <Card blockitem={item} />;

      case "2":
        return <TestBlock_2 blockitem={item} />;

      case "3":
        return <TestBlock_3 blockitem={item} />;

      case "4":
        return <TestBlock_4 blockitem={item} />;

      case "5":
        return <TestBlock_5 blockitem={item} />;

      default:
        break;
    }
  });
  return (
    <Layout>
      <Hero />
      <Process />
      {type}
      {/* 
      <section>

        <Card
          onClick={toggle}
          title='Smarter Golf Club Technology'
          text='The leading provider of full suite membership and club management solutions to country, golf, city, yacht and other private clubs.'
          img='clubessential_laptop.png'
          imgTabPro='clubessential_laptop.png'
          imgTab='clubessential_tablet.png'
          imgTabMob='clubessential_tablet.png'
          imgMob='clubessential_mobile.png'
          hashtag='# sport'
          bgColorClass='lightGreen'
        />
        <Cards
          contentLeft={
            <BoxWithTitle
              onClick={toggle}
              title='Tax Free App'
              text='Best experience at every stage of your tax free shopping journey.'
              hashtag='# service'
              img='taxfree_laptop.png'
              imgTab='taxfree_tablet.png'
              imgMob='taxfree_mobile.png'
              bgColorClass='darkGreen'
              wrapBoxBgPositionStyle=''
            />
          }
          contentRight={
            <>
              <Box
                onClick={toggle}
                title={
                  <>
                    Ultrasonic beauty tool <br /> designed to deliver facial{' '}
                    <br /> spa treatments.
                  </>
                }
                hashtag='# healthcare'
                img='angeltouch_laptop.png'
                imgTab='angeltouch_tablet.png'
                imgMob='angeltouch_mobile.png'
                bgColorClass='pink'
                wrapBoxBgPositionStyle='widthCover'
              />
              <Box
                onClick={toggle}
                title='Artificial intelligence to filter out manual work a recruiter has to perform.'
                hashtag='# crm'
                img='octavia_laptop.png'
                imgTab='octavia_tablet.png'
                imgMob='octavia_mobile.png'
                bgColorClass='darkBlue'
                wrapBoxBgPositionStyle='widthCover'
              />
            </>
          }
        />
        <Card
          onClick={toggle}
          title='Replaces your electricity supplier with technology'
          text={
            <>
              A step forward to smart electricity <br /> consumption and fair
              prices.
            </>
          }
          img='tibber_laptop.png'
          imgTab='tibber_tablet.png'
          imgMob='tibber_mobile.png'
          hashtag='# smarthome'
          bgColorClass='lightPurpure'
        />
        <Cards
          contentLeft={
            <Box
              onClick={toggle}
              title={
                <>
                  A time zone converter, <br /> and an online meeting <br />{' '}
                  scheduler
                </>
              }
              hashtag='# utility'
              img='timezone_laptop.png'
              imgTab='timezone_tablet.png'
              imgMob='timezone_mobile.png'
              bgColorClass='black'
              wrapBoxBgPositionStyle='widthCover'
            />
          }
          contentRight={
            <Box
              onClick={toggle}
              title={
                <>
                  Auto <br /> Repair <br /> Estimator
                </>
              }
              hashtag='# auto'
              img='auto_repair_estimator_laptop.png'
              imgTab='auto_repair_estimator_tablet.png'
              imgMob='auto_repair_estimator_mobile.png'
              bgColorClass='blue'
              wrapBoxBgPositionStyle='widthCover'
            />
          }
        />
        <Cards
          contentLeft={
            <>
              <Box
                onClick={toggle}
                title={
                  <>
                    Anti-Counterfeiting <br /> Technology
                  </>
                }
                hashtag='# branding'
                img='home-branding.png'
                imgTab='home-branding.png'
                imgMob='home-branding.png'
                bgColorClass='blackBlue'
                wrapBoxBgPositionStyle='centerPosition'
              />
              <Box
                onClick={toggle}
                title='Hiring regular car commuters to run a marketing campaign.'
                hashtag='# crm'
                img='airmarketer_laptop.png'
                imgTab='airmarketer_tablet.png'
                imgMob='airmarketer_mobile.png'
                bgColorClass='lightPink'
                wrapBoxBgPositionStyle='widthCover'
              />
            </>
          }
          contentRight={
            <BoxWithTitle
              onClick={toggle}
              title={
                <>
                  Collaboration <br className='hidden-xs' /> Platform for Middle{' '}
                  <br className='hidden-xs' /> East and Africa
                </>
              }
              hashtag='# platform'
              img='home-platform.svg'
              imgTab='home-platform.svg'
              imgMob='home-platform.svg'
              bgColorClass='green'
              wrapBoxBgPositionStyle='centerPosition'
            />
          }
        />
        <Cards
          contentLeft={
            <BoxWithTitle
              onClick={toggle}
              title={
                <>
                  Countdown Timer <br /> to Any Date
                </>
              }
              hashtag='# utility'
              img='countdown_laptop.png'
              imgTab='countdown_tablet.png'
              imgMob='countdown_mobile.png'
              bgColorClass='white'
              wrapBoxBgPositionStyle='centerPosition'
            />
          }
          contentRight={
            <>
              <Box
                onClick={toggle}
                title='Find a parking space in less than 5 minutes'
                hashtag='# parcing'
                img='navigap_laptop.png'
                imgTab='navigap_tablet.png'
                imgMob='navigap_mobile.png'
                bgColorClass='turquoise'
                wrapBoxBgPositionStyle='widthCover'
              />
              <Box
                onClick={toggle}
                title={
                  <>
                    Lawful <br /> Islamic <br /> Foods
                  </>
                }
                hashtag='# food delivery'
                img='halaal_laptop.png'
                imgTab='halaal_tablet.png'
                imgMob='halaal_mobile.png'
                bgColorClass='mint'
                wrapBoxBgPositionStyle=''
              />
            </>
          }
        />
        <Card
          onClick={toggle}
          title={
            <>
              Contactless <br /> visitor <br /> management <br /> system
            </>
          }
          text='The world has changed, and so did office meetings.'
          img='sendpass_laptop.png'
          imgTab='sendpass_tablet.png'
          imgMob='sendpass_mobile.png'
          hashtag='# CRM'
          bgColorClass='darkPurpure'
        />
        <Cards
          contentLeft={
            <>
              <Box
                onClick={toggle}
                title={
                  <>
                    Translation <br /> Platform
                  </>
                }
                hashtag='# platform'
                img='slovotvir_laptop.png'
                imgTab='slovotvir_tablet.png'
                imgMob='slovotvir_mobile.png'
                bgColorClass='blueLight'
                wrapBoxBgPositionStyle='widthCover'
              />
              <Box
                onClick={toggle}
                title='New approach to meet new people through the game.'
                hashtag='# social game'
                img='viche_laptop.png'
                imgTab='viche_tablet.png'
                imgMob='viche_mobile.png'
                bgColorClass='lightPinkRed'
                wrapBoxBgPositionStyle='widthCover'
              />
            </>
          }
          contentRight={
            <BoxWithTitle
              onClick={toggle}
              title='On-demand Lawn, Leaf, and Snow removal'
              hashtag='# service'
              img='home-service.svg'
              imgTab='home-service.svg'
              imgMob='home-service.svg'
              bgColorClass='blueDarkPurpure'
              wrapBoxBgPositionStyle='centerPosition'
            />
          }
        />
        <Card
          onClick={toggle}
          title={
            <>
              New <br /> Era <br />
              Messenger
            </>
          }
          text='Users can send a tic-tac-toe invitation and play it in a real-time mode.'
          img='messenger_laptop.png'
          imgTab='messenger_tablet.png'
          imgMob='messenger_mobile.png'
          hashtag='# messenger'
          bgColorClass='yellow'
        />
        <Cards
          contentLeft={
            <BoxWithTitle
              onClick={toggle}
              title={
                <>
                  Photo sharing <br /> and editing
                </>
              }
              hashtag='# editor'
              img='photo_sharing_and_editing_laptop.png'
              imgTab='photo_sharing_and_editing_tablet.png'
              imgMob='photo_sharing_and_editing_mobile.png'
              bgColorClass='sky'
              wrapBoxBgPositionStyle=''
            />
          }
          contentRight={
            <>
              <Box
                onClick={toggle}
                title='Social network application for patients suffering from severe diseases'
                hashtag='# social network'
                img='sn_in_medicine_laptop.png'
                imgTab='sn_in_medicine_tablet.png'
                imgMob='sn_in_medicine_mobile.png'
                bgColorClass='lightGrey'
                wrapBoxBgPositionStyle='widthCover'
              />
              <Box
                onClick={toggle}
                title='A new look of the legendary game'
                hashtag='# game'
                img='minesweeper_laptop.png'
                imgTab='minesweeper_tablet.png'
                imgMob='minesweeper_mobile.png'
                bgColorClass='lightBlack'
                wrapBoxBgPositionStyle='widthCover'
              />
            </>
          }
        />
        <Cards
          contentLeft={
            <>
              <Box
                onClick={toggle}
                title={
                  <>
                    Software <br /> Development <br /> Company
                  </>
                }
                hashtag='# auto'
                img='home-auto-2.svg'
                imgTab='home-auto-2.svg'
                imgMob='home-auto-2.svg'
                bgColorClass='red'
                wrapBoxBgPositionStyle='centerPosition'
              />
              <Box
                onClick={toggle}
                title={
                  <>
                    Auto <br /> Repair <br /> Estimator
                  </>
                }
                hashtag='# auto'
                img='vehicle_info_laptop.png'
                imgTab='vehicle_info_tablet.png'
                imgMob='vehicle_info_mobile.png'
                bgColorClass='violet'
                wrapBoxBgPositionStyle='widthCover'
              />
            </>
          }
          contentRight={
            <BoxWithTitle
              onClick={toggle}
              title={
                <>
                  Platform for drivers <br /> and passengers to meet and
                  organize their joint trip.
                </>
              }
              hashtag='# platform'
              img='carsharing_laptop.png'
              imgTab='carsharing_tablet.png'
              imgMob='carsharing_mobile.png'
              bgColorClass='lightGreyMilk'
              wrapBoxBgPositionStyle='centerPosition'
            />
          }
        />
      </section>
      <Branding />
      <section>
        <Cards
          contentLeft={
            <BoxWithTitle
              onClick={toggle}
              title={
                <>
                  Your <br /> Product <br />
                  Fingerprint
                </>
              }
              text='Protect your valuable items from counterfeiting using our Unique Authentication Tag and the Blockchain.'
              hashtag='# security'
              img='home-project.svg'
              imgTab='home-project.svg'
              imgMob='home-project.svg'
              bgColorClass='purpureDark'
              wrapBoxBgPositionStyle='centerPosition'
            />
          }
          contentRight={
            <>
              <Box
                onClick={toggle}
                title='Mobile app to balance check gift cards.'
                hashtag='# crm'
                img='doromono_laptop.png'
                imgTab='doromono_tablet.png'
                imgMob='doromono_mobile.png'
                bgColorClass='blueLightSecond'
                wrapBoxBgPositionStyle='centerPosition'
              />
              <Box
                onClick={toggle}
                title='New type of bank set up to compete with large, traditional, national banks.'
                hashtag='# fintech'
                img='runafa_laptop.png'
                imgTab='runafa_tablet.png'
                imgMob='runafa_mobile.png'
                bgColorClass='yellow'
                wrapBoxBgPositionStyle=''
              />
            </>
          }
        />
      </section>
      <List />
      <Form /> */}
      {/* {isShowing && <Modal onClick={toggle} />} */}
    </Layout>
  );
};

export default IndexPage;

// export const query = graphql`
//   query Types {
//     allMarkdownRemark {
//       edges {
//         node {
//           id
//           frontmatter {
//             type
//             title
//           }
//         }
//       }
//     }
//   }
// `;
export const query = graphql`
  query Types {
    allMarkdownRemark {
      edges {
        node {
          id
          frontmatter {
            projects {
              title
              type
              bgColorClass
              hashtag
              imgMob
              img
              imgTab
              imgTabMob
              imgTabPro
              text
            }
          }
        }
      }
    }
  }
`;

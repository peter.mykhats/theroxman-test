import React, { useEffect } from "react";
import { render } from "react-dom";
import cms from "netlify-cms-app";
import {
  InlineSelectControl,
  InlineSelectPreview
} from "netlify-cms-widget-inline-select";

const createRoot = () => {
  const $root = document.createElement("div");
  document.body.appendChild($root);
  return $root;
};

const CMS = () => {
  useEffect(() => {
    cms.registerWidget(
      "inline-select",
      InlineSelectControl,
      InlineSelectPreview
    );
    cms.init();
  }, []);

  return <div id="nc-root"></div>;
};

render(<CMS />, createRoot());

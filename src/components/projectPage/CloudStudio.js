import React from 'react'
import ContactUs from './ContactUs'

const CloudStudio = () => {
  return (
    <section>
      <div className='container'>
        <div className='projectWrapContent'>
          <h2 className='h2'>Cloud studio</h2>
          <p1 className='p1'>
            360 professional camera offers an internal service that allows
            copying and sending all the video to the server, where files are
            being processed and kept for the user's next steps.
          </p1>
        </div>
      </div>
      <div className='wrapImageBoxes'>
        <div className='wrapImageBoxes-item'>
          <picture>
            <source
              srcset={'/img/project/07_laptop.png'}
              media='(min-width: 1200px)'
            />
            <source
              srcset={'/img/project/07_tablet.png'}
              media='(min-width: 768px)'
            />
            <img src={'/img/project/07_mobile.png'} alt='theroxman' />
          </picture>
        </div>
        <div className='wrapImageBoxes-item wrapImageBoxes-item-changeUi'>
          <picture>
            <source
              srcset={'/img/project/08_laptop.png'}
              media='(min-width: 1200px)'
            />
            <source
              srcset={'/img/project/08_tablet.png'}
              media='(min-width: 768px)'
            />
            <img src={'/img/project/08_mobile.png'} alt='theroxman' />
          </picture>
        </div>
      </div>
      <div className='container'>
        <div className='projectWrapContent'>
          <h2 className='h2'>Cloud studio</h2>
          <p1 className='p1'>
            360 professional camera offers an internal service that allows
            copying and sending all the video to the server, where files are
            being processed and kept for the user's next steps.
          </p1>
        </div>
        <div className='wrapImageBoxes-item wrapImageBoxes-item-update'>
          <picture>
            <source
              srcset={'/img/project/09_laptop.png'}
              media='(min-width: 1200px)'
            />
            <source
              srcset={'/img/project/09_tablet.png'}
              media='(min-width: 768px)'
            />
            <img src={'/img/project/09_mobile.png'} alt='theroxman' />
          </picture>
        </div>
        <ContactUs />
      </div>
    </section>
  )
}

export default CloudStudio

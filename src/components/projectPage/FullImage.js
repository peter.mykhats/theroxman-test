import React from 'react'

const FullImage = ({ img, imgTab, imgMob }) => {
  return (
    <section className='fullImageWrap'>
      <picture>
        <source srcset={`/img/project/${img}`} media='(min-width: 1200px)' />
        <source srcset={`/img/project/${imgTab}`} media='(min-width: 768px)' />
        <img src={`/img/project/${imgMob}`} alt='theroxman' />
      </picture>
    </section>
  )
}

export default FullImage

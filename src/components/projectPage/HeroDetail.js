import React from 'react'

const HeroDetail = () => {
  return (
    <section className='heroDedail'>
      <div className='container'>
        <h1 className='h1'>
          360 Camera <br /> Control
        </h1>
        <div className='heroDedail-container'>
          <div className='heroDedail-box'>
            <p className='p1'>Locations</p>
            <h4 className='h4'>Denmark, Copenhagen</h4>
          </div>
          <div className='heroDedail-box'>
            <p className='p1'>Industry</p>
            <h4 className='h4'>Electronics</h4>
          </div>
        </div>
        <div className='heroDedail-wrapText'>
          <p className='p1'>
            Ready to open brand new horizons? 360 professional camera along with
            the IOS app provides the sense of immersion, as it enhances the user
            experience, creates live simulations, visualizes immovable objects
            and allows capturing events as if you were there by your own.
          </p>
        </div>
        <div className='projectWrapImg'>
          <picture>
            <source
              srcset={'/img/project/hero-detail.png'}
              media='(min-width: 1200px)'
            />
            <source
              srcset={'/img/project/01_tablet.png'}
              media='(min-width: 768px)'
            />
            <img src={'/img/project/01_mobile.png'} alt='theroxman' />
          </picture>
        </div>
      </div>
    </section>
  )
}

export default HeroDetail

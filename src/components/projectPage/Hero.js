import React from 'react'

const Hero = () => {
  return (
    <section className='wrapHero wrapHero-update'>
      <header className='container'>
        <a href='/' className='logoLink'>
          <img
            src={'/img/logo_theroxman.svg'}
            alt='theroxman'
            className='logoImg'
          />
        </a>
      </header>
      <picture>
        <source
          srcset={'/img/project/header_laptop.png'}
          media='(min-width: 1200px)'
        />
        <source
          srcset={'/img/project/header_tablet.png'}
          media='(min-width: 768px)'
        />
        <img
          src={'/img/project/header_mobile.png'}
          alt='theroxman'
          className='wrapHero-bg'
        />
      </picture>
    </section>
  )
}

export default Hero

import React from 'react'

const CameraInteraction = () => {
  return (
    <section className='container'>
      <div className='projectWrapContent'>
        <p1 className='p1'>
          The application interface is done in such a way that camera occupies
          maximum screen space, so the user can fully enjoy mobile phone display
          features (all icons appear on transparent elements over the camera
          image).
        </p1>
      </div>
      <div className='projectWrapImg projectWrapImg-update'>
        <picture>
          <source
            srcset={'/img/project/05_laptop.png'}
            media='(min-width: 1200px)'
          />
          <source
            srcset={'/img/project/05_tablet.png'}
            media='(min-width: 768px)'
          />
          <img src={'/img/project/05_mobile.png'} alt='theroxman' />
        </picture>
      </div>
      <div className='projectWrapContent'>
        <h2 className='h2'>
          Camera <br /> interaction
        </h2>
        <p1 className='p1'>
          The user can quickly switch between all the cameras and adjust the
          necessary setting of every camera independently.
        </p1>
      </div>
    </section>
  )
}

export default CameraInteraction

import React from 'react'

const Philosophy = () => {
  return (
    <section className='container'>
      <div className='projectWrapContent'>
        <h2 className='h2'>Philosophy</h2>
        <p1 className='p1'>
          Let's speak about several settings options available with the use of
          the mobile app. Every camera objective can be customized in a
          real-time mode. Moreover, you can control white-balance and exposure
          of every camera independently.
        </p1>
      </div>
      <div className='projectWrapImg projectWrapImg-update'>
        <picture>
          <source
            srcset={'/img/project/03_laptop.png'}
            media='(min-width: 1200px)'
          />
          <source
            srcset={'/img/project/03_tablet.png'}
            media='(min-width: 768px)'
          />
          <img src={'/img/project/03_mobile.png'} alt='theroxman' />
        </picture>
      </div>
      <div className='projectWrapContent'>
        <h2 className='h2'>Usertest</h2>
        <p1 className='p1'>
          Testing is an essential part of the design process. Our obligatory
          internal testing includes reduction of the user path to the main
          functions and every screen analysis as to make the number of elements
          optimal.
        </p1>
      </div>
    </section>
  )
}

export default Philosophy

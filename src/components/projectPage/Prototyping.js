import React from 'react'

const Prototyping = () => {
  return (
    <section className='prototypingWrap'>
      <div className='container'>
        <div className='projectWrapContent'>
          <h2 className='h2'>Prototyping</h2>
          <p1 className='p1'>
            This stage remains one of the most important steps in our work. We
            have started out by developing userflow in order to see and
            understand philosophy (meaning operating principle) of this
            application. As a result, we were able to optimize certain steps and
            create related patterns throughout the application.
          </p1>
        </div>
        <div className='projectWrapImg'>
          <picture>
            <source
              srcset={'/img/project/02_laptop.png'}
              media='(min-width: 1200px)'
            />
            <source
              srcset={'/img/project/02_tablet.png'}
              media='(min-width: 768px)'
            />
            <img src={'/img/project/02_mobile.png'} alt='theroxman' />
          </picture>
        </div>
      </div>
    </section>
  )
}

export default Prototyping

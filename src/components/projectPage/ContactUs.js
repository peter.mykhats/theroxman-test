import { Link } from 'gatsby'
import React, { useState } from 'react'
import Modal from '../modules/Modal'

const ContactUs = () => {
  const [isShowing, setIsShowing] = useState(false)
  const toggle = () => {
    setIsShowing(!isShowing)
  }
  return (
    <footer className='contactInfo'>
      <div className='projectWrapContent'>
        <h2 className='h2'>
          Want to work <br /> with us?
        </h2>
        <p1 className='p1'>
          Are you working on something great? <br /> We'd love to help make it
          happen. <br /> <span onClick={toggle}>Estimate Your Project</span>
        </p1>
      </div>
      <a href='mailto:hello@theroxman.com' className='p1 link'>
        <img src={'/img/project/mail.svg'} alt='theroxman' />
        <span>hello@theroxman.com</span>
      </a>
      <div className='iconList'>
        <Link to='/' className='iconList-link'>
          <img src={'/img/project/1.svg'} alt='theroxman' />
        </Link>
        <Link to='/' className='iconList-link'>
          <img src={'/img/project/2.svg'} alt='theroxman' />
        </Link>
        <Link to='/' className='iconList-link'>
          <img src={'/img/project/3.svg'} alt='theroxman' />
        </Link>
      </div>
      {isShowing && <Modal onClick={toggle} />}
    </footer>
  )
}

export default ContactUs

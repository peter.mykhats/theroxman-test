import React from 'react'

const data = [
  {
    title: 'Research',
    description:
      'A thorough research to understand clearly the needs and preferences for the product.',
  },
  {
    title: 'Ideation',
    description: 'Based on discovery, we generate ideas and set priorities.',
  },
  {
    title: 'Prototyping',
    description:
      'Information Architecture and Wireframes help us visualize the ideas and test them.',
  },
  {
    title: 'Visualization',
    description:
      'We create the UI of key screens and then design all the following screens.',
  },
]

const Process = () => {
  return (
    <section className='container'>
      <h4 className='h4 h4-update'>
        Design process helps us helps <br className='hidden-xs' /> create neat,
        user-friendly mobile <br className='hidden-xs' /> apps for iOS and
        Android platforms.
      </h4>
      <div className='cardList'>
        {data.map((item, index) => (
          <div className='cardList-item' key={index}>
            <span className='h2 cardList-item-number'>{index + 1}</span>
            <h5 className='h5'>{item.title}</h5>
            <p className='p2 cardList-item-description'>{item.description}</p>
          </div>
        ))}
      </div>
    </section>
  )
}

export default Process

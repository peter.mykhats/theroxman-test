import React from 'react'

const Form = () => {
  return (
    <footer className='wrapForm'>
      <div className='container'>
        <h2 className='h2'>Contacts</h2>
        <div className='wrapForm-content'>
          {/* <form className='wrapForm-content-box form'>
            <input
              type='p1'
              name='name'
              placeholder='Name'
              className='form-input'
            />
            <input
              type='p1'
              name='email'
              placeholder='Email'
              className='form-input'
            />
            <label className='form-input'>Message</label>
            <textarea
              name='message'
              className='form-textarea'
              placeholder='Contact us for more information on our services, to get answers to questions or to order a design project.'
            />
            <button className='link form-btn'>
              <img src={'/img/mail.svg'} alt='theroxman' />
              <span>Send Message</span>
            </button>
          </form> */}
          <div className='wrapForm-content-box'>
            <p className='p1'>
              Contact us for more information on our services, to get answers to
              questions or to order a design project.
            </p>
            <a href='mailto:hello@theroxman.com' className='link'>
              hello@theroxman.com
            </a>
            <div className='iconList'>
              <a href='/' className='iconList-link'>
                <img src={'/img/icon/1.svg'} alt='theroxman' />
              </a>
              <a href='/' className='iconList-link'>
                <img src={'/img/icon/2.svg'} alt='theroxman' />
              </a>
              <a href='/' className='iconList-link'>
                <img src={'/img/icon/3.svg'} alt='theroxman' />
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Form

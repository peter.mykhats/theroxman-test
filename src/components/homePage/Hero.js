import React from 'react'

const Hero = () => {
  return (
    <>
      <section className='wrapHero'>
        <div className='container'>
          <header>
            <a href='/' className='logoLink'>
              <img
                src={'/img/logo_theroxman.svg'}
                alt='theroxman'
                className='logoImg'
              />
            </a>
          </header>
          <div className='heroBox'>
            <h1 className='h1'>
              UI/UX Design <br /> Agency focused <br /> on Mobile Interface
            </h1>
            <div className='wrapText'>
              <p className='p1'>
                Our team helps our clients create neat, user-friendly mobile
                apps for iOS and Android platforms.
              </p>
            </div>
            <a href='mailto:hello@theroxman.com' className='p1 link'>
              <img src={'/img/mail.svg'} alt='theroxman' />
              <span>hello@theroxman.com</span>
            </a>
          </div>
        </div>
        <picture>
          <source
            srcSet={'/img/header_laptop.png'}
            media='(min-width: 960px)'
          />
          <source
            srcSet={'/img/header_tablet.png'}
            media='(min-width: 574px)'
          />
          <img
            src={'/img/header_mobile.png'}
            alt='theroxman'
            className='wrapHero-bg'
          />
        </picture>
      </section>
    </>
  )
}

export default Hero

import React from "react";
import Cards from "../modules/Cards";
import Box from "../modules/Box";

const TestBlock_2 = ({ blockitem }) => {
  console.log("Component - blockitem:::", blockitem);
  return (
    <Cards
      contentLeft={
        <Box
          title={
            <>
              A time zone converter, <br /> and an online meeting <br />{" "}
              scheduler
            </>
          }
          hashtag="# utility"
          img="timezone_laptop.png"
          imgTab="timezone_tablet.png"
          imgMob="timezone_mobile.png"
          bgColorClass="black"
          wrapBoxBgPositionStyle="widthCover"
        />
      }
      contentRight={
        <Box
          title={
            <>
              Auto <br /> Repair <br /> Estimator
            </>
          }
          hashtag="# auto"
          img="auto_repair_estimator_laptop.png"
          imgTab="auto_repair_estimator_tablet.png"
          imgMob="auto_repair_estimator_mobile.png"
          bgColorClass="blue"
          wrapBoxBgPositionStyle="widthCover"
        />
      }
    />
  );
};

export default TestBlock_2;

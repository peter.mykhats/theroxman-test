import React from 'react'

const data = [
  'apple',
  'android',
  'zeplin',
  'invisio',
  'principle',
  'figma',
  'sketch',
  'adobe_cc',
  'balsamiq',
  'axure',
]

const List = () => {
  return (
    <section className='container'>
      <h4 className='h4 h4-update maxWidthMdl'>
        <span>We offer a choice of</span> technology <span>from our</span>{' '}
        toolkit <span>to serve the project best in</span> design{' '}
        <span>and</span> prototyping.
      </h4>
      <ul className='technologyList'>
        {data.map((item) => (
          <li className='technologyList-item' key={item.toString()}>
            <img src={`/img/technology/${item}.svg`} alt={item} />
          </li>
        ))}
      </ul>
      <p className='technologyList-text p1'>
        Let us know, in case you haven`t spotted the technology you had in mind.{' '}
        <a href='/'>Contact us.</a>
      </p>
    </section>
  )
}

export default List

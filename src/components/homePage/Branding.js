import React from 'react'

const Branding = () => {
  return (
    <section className='wrapBranding'>
      <div className='container'>
        <h2 className='h2'>
          Branding <br /> Product
        </h2>
        <p className='p1'>
          Your app may be amazing, but users need to take notice of it first!{' '}
          <br /> A gorgeous, effective logo and App Store Screenshots will
          ensure your app gets noticed and downloaded.
        </p>
      </div>
      <picture>
        <source srcSet={'/img/home-sets.png'} media='(min-width: 1200px)' />
        <source srcSet={'/img/home-sets-tab.png'} media='(min-width: 768px)' />
        <img
          src={'/img/home-sets-mob.png'}
          alt='theroxman'
          className='wrapBranding-bg'
        />
      </picture>
    </section>
  )
}

export default Branding

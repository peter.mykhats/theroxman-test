import React from "react";

const Card = ({
  // title,
  // text,
  // img,
  // imgTabPro,
  // imgTab,
  // imgTabMob,
  // imgMob,
  // hashtag,
  // bgColorClass,
  // onClick,
  blockitem
}) => {
  console.log("data in card::", blockitem);
  return (
    <div className={`wrapOneCard ${blockitem.bgColorClass}`}>
      <picture>
        <source srcSet={`/img/${blockitem.img}`} media="(min-width: 1200px)" />
        {blockitem.imgTabPro && (
          <source
            srcSet={`/img/${blockitem.imgTabPro}`}
            media="(min-width: 960px)"
          />
        )}
        <source
          srcSet={`/img/${blockitem.imgTab}`}
          media="(min-width: 768px)"
        />
        {blockitem.imgTabMob && (
          <source
            srcSet={`/img/${blockitem.imgTab}`}
            media="(min-width: 574px)"
          />
        )}
        <img
          src={`/img/${blockitem.imgMob}`}
          alt="theroxman"
          className="wrapOneCard-bg"
        />
      </picture>
      <div className="container">
        <div className="wrapCardTitle">
          <h2 className="h2">{blockitem.title}</h2>
        </div>
        <div className="wrapCardText">
          <p className="p1">{blockitem.text}</p>
        </div>
        <span className="hashtag">{blockitem.hashtag}</span>
      </div>
    </div>
  );
};

export default Card;

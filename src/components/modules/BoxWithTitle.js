import React from 'react'

const BoxWithTitle = ({
  onClick,
  title,
  text,
  img,
  imgTab,
  imgMob,
  hashtag,
  bgColorClass,
  wrapBoxBgPositionStyle,
}) => {
  return (
    <div
      className={`wrapBox wrapBox--height ${bgColorClass}`}
      onClick={onClick}
    >
      <div className='wrapCard-box-title'>
        <h4 className='h4'>{title}</h4>
        <p className='p1'>{text}</p>
      </div>
      <span className='hashtag'>{hashtag}</span>
      <picture>
        <source srcSet={`/img/box/${img}`} media='(min-width: 1200px)' />
        <source srcSet={`/img/box/${imgTab}`} media='(min-width: 768px)' />
        <img
          src={`/img/box/${imgMob}`}
          alt='theroxman'
          className={`wrapBox-bg ${wrapBoxBgPositionStyle}`}
        />
      </picture>
    </div>
  )
}

export default BoxWithTitle

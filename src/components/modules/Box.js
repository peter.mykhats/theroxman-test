import React from "react";

const Box = ({
  // onClick,
  title,
  img,
  imgTab,
  imgMob,
  hashtag,
  bgColorClass,
  wrapBoxBgPositionStyle
}) => {
  return (
    <div className={`wrapBox ${bgColorClass}`}>
      <div className="wrapBoxTitle">
        <h5 className="h5">{title}</h5>
      </div>
      <span className="hashtag">{hashtag}</span>
      <picture>
        <source srcSet={`/img/box/${img}`} media="(min-width: 1200px)" />
        <source srcSet={`/img/box/${imgTab}`} media="(min-width: 768px)" />
        <img
          src={`/img/box/${imgMob}`}
          alt="theroxman"
          className={`wrapBox-bg ${wrapBoxBgPositionStyle}`}
        />
      </picture>
    </div>
  );
};

export default Box;

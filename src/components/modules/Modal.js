import { Link } from 'gatsby'
import React from 'react'

const Modal = ({ onClick }) => {
  return (
    <div className='modal'>
      <div className='container'>
        <img
          src={'/img/logo-icon.svg'}
          alt='theroxman'
          className='modal-logo'
        />
        <div className='modal-body'>
          <p className='modal-title'>The project is still in progress.</p>
          <p className='modal-description'>
            If you’d like to see it sooner, please email us:
            <a href='mailto:hello@theroxman.com' className='link'>
              <span>hello@theroxman.com</span>
            </a>
          </p>
          {/* <form className='form'>
            <input
              type='p1'
              name='email'
              placeholder='Type your email'
              className='form-input'
            />
            <button className='form-btn'>Get Access</button>
          </form> */}
        </div>
      </div>
      <div className='modal-wrapCloseIcon' onClick={onClick}>
        <img src={'/img/close.svg'} alt='theroxman' />
      </div>
    </div>
  )
}

export default Modal

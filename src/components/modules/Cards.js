import React from 'react'

const Cards = ({ contentLeft, contentRight }) => {
  return (
    <div className='wrapCard'>
      <div className='wrapCard-box'>{contentLeft}</div>
      <div className='wrapCard-box'>{contentRight}</div>
    </div>
  )
}

export default Cards

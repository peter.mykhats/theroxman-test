module.exports = {
  siteMetadata: {
    title: `the Roxman`,
  },
  plugins: [
    `gatsby-plugin-favicon`,
    `gatsby-plugin-sass`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `types`,
        path: `${__dirname}/content`,
      },
    },
    {
      resolve: `gatsby-plugin-clicky`,
      options: {
        siteId: '101172283',
      },
    },
    // {
    //   resolve: `gatsby-plugin-favicon`,
    //   options: {
    //     logo: './src/favicon.png',

    //     // WebApp Manifest Configuration
    //     appName: 'the Roxman', // Inferred with your package.json
    //     appDescription: null,
    //     developerName: null,
    //     developerURL: null,
    //     dir: 'auto',
    //     lang: 'en-US',
    //     background: '#fff',
    //     theme_color: '#fff',
    //     display: 'standalone',
    //     orientation: 'any',
    //     start_url: '/?homescreen=1',
    //     version: '1.0',

    //     icons: {
    //       android: true,
    //       appleIcon: true,
    //       appleStartup: true,
    //       coast: false,
    //       favicons: true,
    //       firefox: true,
    //       yandex: false,
    //       windows: false,
    //     },
    //   },
    // },
  ],
}
